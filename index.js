(function(){

    var BookApp = angular.module("BookApp", ["ui.router"]);

    var BookConfig = function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("booklist", {
                url: "/booklist",
                templateUrl: "/views/booklist.html",
                controller: "BookCtrl as bookCtrl"
            })
        
            .state("reviews",{
                url: "/reviews/:title",
                templateUrl: "/views/reviews.html",
                controller: "GetReviewCtrl as getReviewCtrl"
            })
            
            $urlRouterProvider.otherwise("/booklist");
    } //BookConfig
    BookConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];

    var BookSvc = function($http,$q){
        var bookSvc = this;

        bookSvc.getBookList = function(url,key) {
            var defer = $q.defer();
            //var url = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
            $http.get(url, {
                params: { "api-key": key }
            }).then(function(result){
                //console.log("result >>>>");
                console.log(result);
                //var overview = result.data.results.lists[2].books;
                var overview = result.data.results.lists;
                defer.resolve(overview);
            }).catch(function(err){
                defer.reject(err);
            })
            return (defer.promise);
        }
            //http://idreambooks.com/api/books/show_features.{format}?q={keywords}&key={yourAPIkey}
            bookSvc.getReviews = function(url,key,title) {
                var defer = $q.defer();
                $http.get(url, {
                    params: { q: title, key: key  }
                }).then(function(result){
                    console.log("result >>>>");
                    console.log(result);
                    if(result.data.total_results > 0 && 
                        result.data.book.critic_reviews.length !=0){
                        var reviews = result.data.book;
                    }
                    else {
                        var reviews = "";
                    }
                    defer.resolve(reviews);
                }).catch(function(err){
                    defer.reject(err);
                })
                return (defer.promise);
        }
    }//BookSvc
    BookSvc.$inject = ["$http", "$q"];

    var BookCtrl = function($state, BookSvc){
        var bookCtrl = this;
        var urlOverview = "https://api.nytimes.com/svc/books/v3/lists/overview.json";
        var key = "9173a4091d6f4e22b81c3f046ac0b45c"; //BookAPI key
        bookCtrl.list = "";

        //console.log("in BookCtrl>>>>");
        BookSvc.getBookList(urlOverview,key)
            .then(function(result){
                
                bookCtrl.list = result;
                console.log(">>> Book List >>> ", bookCtrl.list);
                //console.log(">>> Book List >>> ", result);
            }).catch(function(err){
                console.error(" >>>> getBookList: ", err);
            })
            
            bookCtrl.GetReviews = function(t) {
                console.info(">>>> title = ", t);
                $state.go("reviews", { title: t });
            }
    } //BookCtrl
    BookCtrl.$inject = [ "$state", "BookSvc" ];

    var GetReviewCtrl = function($stateParams, BookSvc){
        var getReviewCtrl = this;
        
        //var urlReviews = "https://api.nytimes.com/svc/books/v3/reviews.json";
        //var key = "9173a4091d6f4e22b81c3f046ac0b45c"; //BookAPI key
        var urlReviews = "http://idreambooks.com/api/books/reviews.json";
        var key = "3a1b9e765bbf570fea3f10ac491d683f5123f9af";
        getReviewCtrl.title = $stateParams.title;
        getReviewCtrl.reviews = [];
        
        console.log(">>>>in GetReviewCtrl>>>");
        //console.log("Title clicked >>> %s", getReviewCtrl.title);
        BookSvc.getReviews(urlReviews,key,getReviewCtrl.title)
        .then(function(result){
            //console.log(">>>result for reviews >>>");
            console.log(result);
            
            getReviewCtrl.reviews = result;
            
        }).catch(function(err){
            console.error(" >>>> getReviews: ", err);
        })
    } //GetReviewCtrl
    GetReviewCtrl.$inject = [ "$stateParams", "BookSvc" ];

    BookApp.config(BookConfig);

    BookApp.service("BookSvc", BookSvc);

    BookApp.controller("BookCtrl", BookCtrl);
    BookApp.controller("GetReviewCtrl", GetReviewCtrl);

})(); //1

//var url = "https://api.nytimes.com/svc/books/v3/lists/overview.json";